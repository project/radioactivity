<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Unit;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\radioactivity\RadioactivityInterface;
use Drupal\radioactivity\RadioactivityLazyBuilder;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\radioactivity\RadioactivityLazyBuilder
 * @group radioactivity
 */
class RadioactivityLazyBuilderTest extends UnitTestCase {
  use ProphecyTrait;

  /**
   * Instance of RadioactivityLazyBuilder for testing.
   *
   * @var \Drupal\radioactivity\RadioactivityLazyBuilder
   */
  protected $lazyBuilder;

  /**
   * Mock entity type manager.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $energy = 9.12345;

    $radioactivityEntity = $this->prophesize(RadioactivityInterface::class);
    $radioactivityEntity->getEnergy()
      ->willReturn($energy);
    $radioactivityEntity->getCacheTags()
      ->willReturn(['radioactivity:1']);

    $radioactivityStorage = $this->prophesize(EntityStorageInterface::class);
    $radioactivityStorage->load(Argument::is(0))
      ->willReturn(NULL);
    $radioactivityStorage->load(Argument::is(1))
      ->willReturn($radioactivityEntity->reveal());

    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->entityTypeManager->getStorage('radioactivity')
      ->willReturn($radioactivityStorage->reveal());

    // Create concrete RadioactivityLazyBuilder with mocked services.
    $this->lazyBuilder = new RadioactivityLazyBuilder($this->entityTypeManager->reveal());
  }

  /**
   * @covers ::trustedCallbacks
   */
  public function testTrustedCallbacks(): void {
    $this->assertEquals(['buildReferencedValue'], $this->lazyBuilder->trustedCallbacks());
  }

  /**
   * @covers ::buildReferencedValue
   * @dataProvider providerBuildReferencedValue
   */
  public function testBuildReferencedValue(int $entityId, ?int $decimals, array $expected): void {
    $this->assertEquals($expected, $this->lazyBuilder->buildReferencedValue($entityId, $decimals));
  }

  /**
   * Data provider for testBuildReferencedValue.
   *
   * @return array
   *   Entity ID, Decimals, Return value of::buildReferencedValue.
   */
  public static function providerBuildReferencedValue(): array {
    return [
      [0, 0, []],
      [1, 0, ['#markup' => '9', '#cache' => ['tags' => ['radioactivity:1']]]],
      [1, 1, ['#markup' => '9.1', '#cache' => ['tags' => ['radioactivity:1']]]],
      [1, 2, ['#markup' => '9.12', '#cache' => ['tags' => ['radioactivity:1']]]],
      [1, NULL, ['#markup' => '9.12345', '#cache' => ['tags' => ['radioactivity:1']]]],
    ];
  }

}
