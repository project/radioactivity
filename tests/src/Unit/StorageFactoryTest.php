<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Unit;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\radioactivity\DefaultIncidentStorage;
use Drupal\radioactivity\IncidentStorageInterface;
use Drupal\radioactivity\RestIncidentStorageInterface;
use Drupal\radioactivity\StorageFactory;

/**
 * @coversDefaultClass \Drupal\radioactivity\StorageFactory
 * @group radioactivity
 */
class StorageFactoryTest extends UnitTestCase {

  /**
   * Instance of StorageFactory for testing.
   *
   * @var \Drupal\radioactivity\StorageFactory
   */
  protected $storageFactory;

  /**
   * Mocked immutable configuration object.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Mocked class resolver.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Mocked config factory.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Mock the radioactivity.storage configuration.
    $this->config = $this->createMock(ImmutableConfig::class);

    $this->configFactory = $this->createMock(ConfigFactory::class);
    $this->configFactory->expects($this->any())
      ->method('get')
      ->willReturn($this->config);

    // Mock the class resolver and the classes it provides.
    $mockRestStorage = $this->createMock(RestIncidentStorageInterface::class);
    $mockDefaultStorage = $this->createMock(DefaultIncidentStorage::class);

    $this->classResolver = $this->createMock(ClassResolverInterface::class);
    $this->classResolver->expects($this->any())
      ->method('getInstanceFromDefinition')
      ->willReturnMap([
        ['radioactivity.rest_incident_storage', $mockRestStorage],
        ['radioactivity.default_incident_storage', $mockDefaultStorage],
      ]);

    // Create concrete StorageFactory with mocked services.
    $this->storageFactory = new StorageFactory($this->configFactory, $this->classResolver);
  }

  /**
   * @covers ::get
   * @dataProvider providerGet
   */
  public function testGet(string $storageType, string $storageClass): void {
    $result = $this->storageFactory->get($storageType);
    $this->assertInstanceOf(IncidentStorageInterface::class, $result);
    $this->assertInstanceOf($storageClass, $result);
  }

  /**
   * Data provider for testGet.
   *
   * @return array
   *   Storage type, storage class.
   */
  public static function providerGet(): array {
    // Format of each element is:
    // - storageType: A string storage type.
    // - storageClass: A string holding a fully qualified class name.
    return [
      ['rest_local', RestIncidentStorageInterface::class],
      ['rest_remote', RestIncidentStorageInterface::class],
      ['default', DefaultIncidentStorage::class],
      ['unknown_type', DefaultIncidentStorage::class],
    ];
  }

  /**
   * @covers ::getConfiguredStorage
   * @dataProvider providerGetConfiguredStorage
   */
  public function testGetConfiguredStorage(?string $configType, string $storageType): void {
    $sut = $this->getMockBuilder(StorageFactory::class)
      ->onlyMethods(['get'])
      ->setConstructorArgs([
        $this->configFactory,
        $this->classResolver,
      ])
      ->getMock();

    $sut->expects($this->once())
      ->method('get')
      ->with($this->equalTo($storageType));
    $this->setConfig($configType, '');

    $sut->getConfiguredStorage();
  }

  /**
   * Data provider for testGetConfiguredStorage.
   *
   * @return array
   *   Configured type, storage type.
   */
  public static function providerGetConfiguredStorage(): array {
    // Format of each element is:
    // - configType: A string configuration type.
    // - storageType: A string storage type.
    return [
      ['rest_local', 'rest_local'],
      ['rest_remote', 'rest_remote'],
      ['default', 'default'],
      [NULL, 'default'],
    ];
  }

  /**
   * Sets mock configuration for StorageFactory.
   *
   * @param string $storageType
   *   The configured storage type.
   * @param string $endpoint
   *   The configured endpoint.
   */
  protected function setConfig(?string $storageType, ?string $endpoint): void {
    $this->config->expects($this->any())
      ->method('get')
      ->willReturnMap([
        ['type', $storageType],
        ['endpoint', $endpoint],
      ]);
  }

}
