<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Unit\Integration\Event;

use Drupal\radioactivity\Event\EnergyBelowCutoffEvent;

/**
 * Checks that the event EnergyBelowCutoffEvent is correctly defined.
 *
 * @coversDefaultClass \Drupal\radioactivity\Event\EnergyBelowCutoffEvent
 * @group radioactivity
 */
class EnergyBelowCutoffTest extends EventTestBase {

  /**
   * Tests the event metadata.
   */
  public function testEnergyBelowCutoffEvent(): void {
    $plugin_definition = $this->eventManager->getDefinition(EnergyBelowCutoffEvent::EVENT_NAME);
    $this->assertSame('Energy is below the cutoff level', (string) $plugin_definition['label']);

    $event = $this->eventManager->createInstance(EnergyBelowCutoffEvent::EVENT_NAME);
    $entity_context_definition = $event->getContextDefinition('entity');
    $this->assertSame('entity', $entity_context_definition->getDataType());
    $this->assertSame('Entity', $entity_context_definition->getLabel());
  }

}
