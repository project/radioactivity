<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Functional;

use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\radioactivity\Entity\Radioactivity;
use Drush\TestTraits\DrushTestTrait;

/**
 * @coversDefaultClass \Drupal\radioactivity\Drush\Commands\RadioactivityDrushCommands
 *
 * @group radioactivity
 */
class RadioactivityDrushCommandsTest extends BrowserTestBase {
  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'radioactivity',
    'node',
  ];

  /**
   * Tests the Drush radioactivity:fix-references command.
   */
  public function testFixReferences(): void {
    // Create nodes with no radioactivity reference fields.
    $node_type = NodeType::create(['type' => 'page', 'name' => 'Page']);
    $node_type->save();
    $node1 = Node::create(['title' => 'Node 1 without radioactivity_reference field', 'type' => 'page']);
    $node1->save();
    $node2 = Node::create(['title' => 'Node 2 without radioactivity_reference field', 'type' => 'page']);
    $node2->save();

    $this->drush('radioactivity:fix-references');
    $expected_output = '[warning] No radioactivity reference fields found that need to be fixed.';
    $this->assertEquals($expected_output, $this->getErrorOutput());

    // Now add a RadioactivityReference field to the node type.
    FieldStorageConfig::create([
      'entity_type' => 'node',
      'type' => 'radioactivity_reference',
      'field_name' => 'ref',
      'cardinality' => 1,
      'settings' => [
        'profile' => 'count',
        'granularity' => 900,
        'halflife' => 43200,
        'cutoff' => 10,
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'page',
      'field_name' => 'ref',
      'required' => TRUE,
      'settings' => [
        'handler' => 'default:radioactivity',
        'handler_settings' => ['auto_create' => FALSE],
        'default_energy' => 100,
      ],
    ])->save();

    // Reload the nodes now that the NodeType has been modified.
    $node1 = Node::load($node1->id());
    $node2 = Node::load($node2->id());

    // Create a new node that will have a radioactivity reference field.
    $node = Node::create([
      'title' => 'Node for radioactivity_reference field content',
      'type' => 'page',
    ]);
    // Note: Shouldn't have to do the following - the referenced entity should
    // be automatically created when the node is created.
    // @see https://www.drupal.org/project/radioactivity/issues/3325229
    $radioactivityEntity = Radioactivity::create([
      'timestamp' => 1234567890,
      'energy' => 100,
      'langcode' => 'en',
    ]);
    $radioactivityEntity->save();
    $node->ref->setValue($radioactivityEntity);
    $node->save();

    // We created 3 'page' nodes above, the first two of which didn't have a
    // radioactivity reference field when they were created. After the field
    // was added to the 'page' bundle, these nodes still had no content in that
    // field.
    //
    // Now we verify that the first two nodes need fixing (i.e., they need to
    // a Radioactivity entity created and stored in their reference field),
    // while the third one doesn't need fixing.
    $this->assertNull($node1->ref->entity);
    $this->assertNull($node2->ref->entity);
    $this->assertEquals($radioactivityEntity, $node->ref->entity);

    // Finally, check that the drush command fixes those first two.
    $this->drush('radioactivity:fix-references');
    $expected_output = '[success] 2 entities with radioactivity reference field fixed.';
    $this->assertEquals($expected_output, $this->getErrorOutput());

    \Drupal::entityTypeManager()->getStorage('node')->resetCache();
    $this->assertInstanceOf(Radioactivity::class, Node::load($node1->id())->ref->entity);
    $this->assertInstanceOf(Radioactivity::class, Node::load($node2->id())->ref->entity);
    $this->assertEquals($radioactivityEntity->id(), Node::load($node->id())->ref->entity->id());
  }

}
