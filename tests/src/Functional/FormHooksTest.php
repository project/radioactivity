<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * @coversDefaultClass \Drupal\radioactivity\src\Hook\RadioactivityFormHooks
 *
 * @group radioactivity
 */
class FormHooksTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administer node fields, etc.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'radioactivity',
    'node',
    'text',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer node fields']);
    $this->drupalLogin($this->adminUser);
    $this->config('system.logging')
      ->set('error_level', ERROR_REPORTING_DISPLAY_ALL)
      ->save();
  }

  /**
   * Tests hook_form_field_config_edit_form_alter().
   */
  public function testHookFormFieldConfigEditFormAlter(): void {
    // Create node type to hold radioactivity reference field.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    /** @var \Drupal\Tests\WebAssert $assert */
    $assert = $this->assertSession();

    // Use the UI to add the field.
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    // Select field group 'Trending'.
    $assert->elementExists('css', "[name='new_storage_type'][value='trending']");
    $edit = ['new_storage_type' => 'trending'];
    $this->submitForm($edit, 'Continue');

    // Verify the form contains an option for the radioactivity reference
    // field, but not for the deprecated radioactivity value field.
    $assert->pageTextContains('Radioactivity Reference');
    $assert->pageTextContains('This field stores the ID of an radioactivity energy entity.');
    $assert->pageTextNotContains('Radioactivity (deprecated)');
    $assert->pageTextNotContains('Radioactivity energy level and energy emitter. Do not use for new sites.');

    // Fill out the form to submit the form which adds the field.
    $label = 'hotness';
    $edit = [
      'label' => $label,
      'field_name' => "$label",
      'group_field_options_wrapper' => 'radioactivity_reference',
    ];
    $this->submitForm($edit, 'Continue');

    // Assert 'required' checkbox is present, checked, and disabled.
    $assert->elementExists('css', "[name='required'][checked='checked'][disabled='disabled']");
    $this->getSession()->getPage()->pressButton('Save settings');

    $assert->pageTextContains("Saved $label configuration.");
    $assert->pageTextContains('Radioactivity Reference');
    $assert->pageTextContains('Reference type: Radioactivity');
  }

  /**
   * Tests hook_form_field_ui_field_storage_add_form_alter().
   *
   * @todo Remove in 5.0.0 when the deprecated 'radioactivity' field gets removed.
   */
  public function testHookFormFieldUiFieldStorageAddFormAlter(): void {
    // Create node type to hold radioactivity reference field.
    $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);

    /** @var \Drupal\Tests\WebAssert $assert */
    $assert = $this->assertSession();

    // Now create an old Radioactivity value field. Have to do this
    // programmatically, because our hook prevent us from doing this in the UI!
    FieldStorageConfig::create([
      'entity_type' => 'node',
      'type' => 'radioactivity',
      'field_name' => 'deprecated',
      'cardinality' => 1,
      'settings' => [
        'profile' => 'count',
        'granularity' => 900,
        'halflife' => 43200,
        'cutoff' => 10,
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => 'node',
      'bundle' => 'page',
      'field_name' => 'deprecated',
      'label' => 'Radioactivity',
      'required' => TRUE,
      'default_value' => [
        [
          'energy' => 100,
          'timestamp' => 0,
        ],
      ],
    ])->save();

    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    // Select field group 'Trending'.
    $assert->elementExists('css', "[name='new_storage_type'][value='trending']");
    $edit = ['new_storage_type' => 'trending'];
    $this->submitForm($edit, 'Continue');

    // Verify the form contains an option for the radioactivity reference
    // field AND a choice for the deprecated radioactivity value field.
    $assert->pageTextContains('Radioactivity Reference');
    $assert->pageTextContains('This field stores the ID of an radioactivity energy entity.');
    $assert->pageTextContains('Radioactivity (deprecated)');
    $assert->pageTextContains('Radioactivity energy level and energy emitter. Do not use for new sites.');
  }

}
