<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\FunctionalJavascript;

use Drupal\Core\Entity\EntityInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\radioactivity\Traits\RadioactivityFunctionTestTrait;

/**
 * Base for Radioactivity functional JavaScript tests.
 */
abstract class RadioactivityFunctionalJavascriptTestBase extends WebDriverTestBase {
  use RadioactivityFunctionTestTrait;

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * The entity that holds the energy field(s).
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'radioactivity',
    'node',
    'field',
    'field_ui',
    'entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create administrative user.
    $this->adminUser = $this->drupalCreateUser([
      'administer entity_test fields',
      'administer entity_test form display',
      'administer entity_test display',
      'administer entity_test content',
      'view test entity',
    ]);
    $this->drupalLogin($this->adminUser);

    // Set default entity type and bundle.
    $this->entityType = 'entity_type';
    $this->entityBundle = 'entity_type';
  }

  /**
   * Assert the energy values from a field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The host entity of the field.
   * @param string $fieldName
   *   The field to be asserted.
   * @param array|string|int $expectedValues
   *   The expected field values.
   * @param string $operator
   *   The operator to be used to compare. Allowed values: '>', '>=', '<', '<=',
   *   '=='. The actual value on the left, the expected on the right.
   * @param string $message
   *   The assertion message.
   */
  public function assertFieldEnergyValue(EntityInterface $entity, string $fieldName, array|string|int $expectedValues, string $operator = '==', string $message = ''): void {
    $expectedValues = is_array($expectedValues) ? $expectedValues : [$expectedValues];
    $actualValues = array_map(
      function ($item) {
        return $item['energy'];
      },
      $entity->get($fieldName)->getValue()
    );

    $this->assertEnergyValues($fieldName, $actualValues, $expectedValues, $operator, $message);
  }

  /**
   * Assert the energy values from the page.
   *
   * @param string $fieldName
   *   The field to be asserted.
   * @param array|string|int $expectedValues
   *   The expected field values.
   * @param string $operator
   *   The operator to be used to compare. Allowed values: '>', '>=', '<', '<=',
   *   '=='. The actual value on the left, the expected on the right.
   * @param string $message
   *   The assertion message.
   */
  public function assertPageEnergyValue(string $fieldName, array|string|int $expectedValues, string $operator = '==', string $message = ''): void {
    $expectedValues = is_array($expectedValues) ? $expectedValues : [$expectedValues];
    $actualValues = $this->getPageEnergyValues($fieldName);

    $this->assertEnergyValues($fieldName, $actualValues, $expectedValues, $operator, $message);
  }

  /**
   * Gets the field's energy values from the session's page.
   *
   * @param string $fieldName
   *   The name of the field to be asserted.
   *
   * @return array
   *   The field values.
   */
  protected function getPageEnergyValues(string $fieldName): array {
    $values = [];
    $fieldBaseName = substr($fieldName, 6);
    $selector = '.field--name-field-' . $fieldBaseName . ' .field__item';

    $rows = $this->getSession()->getPage()->findAll('css', $selector);
    if ($rows) {
      foreach ($rows as $row) {
        $values[] = $row->getHtml();
      }
    }

    return $values;
  }

  /**
   * Assert field energy values.
   *
   * @param string $fieldName
   *   The field to be asserted.
   * @param array $actualValues
   *   The actual field values.
   * @param array $expectedValues
   *   The expected field values.
   * @param string $operator
   *   The operator to be used to compare. Allowed values: '>', '>=', '<', '<=',
   *   '=='. The actual value on the left, the expected on the right.
   * @param string $message
   *   The assertion message.
   */
  protected function assertEnergyValues(string $fieldName, array $actualValues, array $expectedValues, string $operator = '==', string $message = ''): void {
    if (array_diff(array_keys($actualValues), array_keys($expectedValues))) {
      throw new \RuntimeException(sprintf('Invalid number of expected values for %s.', $fieldName));
    }

    foreach ($actualValues as $key => $actual) {
      $expected = $expectedValues[$key];

      switch ($operator) {
        case '>':
          $result = $actual > $expected;
          break;

        case '>=':
          $result = $actual >= $expected;
          break;

        case '<':
          $result = $actual < $expected;
          break;

        case '<=':
          $result = $actual <= $expected;
          break;

        case '==':
        default:
          $result = $actual == $expected;
      }
      $message = $message ?: $message = sprintf('The energy value of %s is %s, but %s expected.', $fieldName, $actual, $expected);
      $this->assertTrue($result, $message);
    }
  }

}
