<?php

declare(strict_types=1);

namespace Drupal\Tests\radioactivity\Traits;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Radioactivity functional test trait.
 */
trait RadioactivityFunctionTestTrait {

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType = 'entity_test';

  /**
   * The entity type bundle.
   *
   * @var string
   */
  protected $entityBundle = 'entity_test';

  /**
   * Adds a Count type energy field to the content type.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int|float $defaultEnergy
   *   Default energy level.
   * @param int $cardinality
   *   Field cardinality.
   */
  public function addCountEnergyField(string $fieldName, int|float $defaultEnergy = 0, int $cardinality = 1): void {
    $granularity = $halfLifeTime = $cutoff = 0;
    $this->createEnergyField($fieldName, 'count', $defaultEnergy, $granularity, $halfLifeTime, $cutoff, $cardinality);
  }

  /**
   * Adds a Linear type energy field to the content type.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int|float $defaultEnergy
   *   Field energy when the entity is created.
   * @param int $granularity
   *   Energy decay granularity.
   * @param int|float $cutoff
   *   Energy cut off value.
   * @param int $cardinality
   *   Field cardinality.
   */
  public function addLinearEnergyField(string $fieldName, int|float $defaultEnergy = 0, int $granularity = 900, int|float $cutoff = 10, int $cardinality = 1): void {
    $halfLifeTime = 0;
    $this->createEnergyField($fieldName, 'linear', $defaultEnergy, $granularity, $halfLifeTime, $cutoff, $cardinality);
  }

  /**
   * Adds a Decay type energy field to the content type.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int|float $defaultEnergy
   *   Field energy when the entity is created.
   * @param int $granularity
   *   Energy decay granularity.
   * @param int $halfLifeTime
   *   Half-life time.
   * @param int|float $cutoff
   *   Energy cut off value.
   * @param int $cardinality
   *   Field cardinality.
   */
  public function addDecayEnergyField(string $fieldName, int|float $defaultEnergy = 0, int $granularity = 0, int $halfLifeTime = 43200, int|float $cutoff = 10, int $cardinality = 1): void {
    $this->createEnergyField($fieldName, 'decay', $defaultEnergy, $granularity, $halfLifeTime, $cutoff, $cardinality);
  }

  /**
   * Adds an radioactivity energy field to the content type.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param string $profile
   *   Profile type.
   * @param int|float $defaultEnergy
   *   Field energy when the entity is created.
   * @param int $granularity
   *   Energy decay granularity.
   * @param int $halfLifeTime
   *   Half life time.
   * @param int|float $cutoff
   *   Energy cut off value.
   * @param int $cardinality
   *   Field cardinality.
   */
  protected function createEnergyField(string $fieldName, string $profile, int|float $defaultEnergy = 0, int $granularity = 900, int $halfLifeTime = 43200, int|float $cutoff = 10, int $cardinality = 1): void {
    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'type' => 'radioactivity',
      'field_name' => $fieldName,
      'cardinality' => $cardinality,
      'settings' => [
        'profile' => $profile,
        'granularity' => $granularity,
        'halflife' => $halfLifeTime,
        'cutoff' => $cutoff,
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => $this->entityType,
      'bundle' => $this->entityBundle,
      'field_name' => $fieldName,
      'label' => 'Radioactivity',
      'required' => TRUE,
      'default_value' => [
        [
          'energy' => $defaultEnergy,
          'timestamp' => 0,
        ],
      ],
    ])->save();
  }

  /**
   * Adds an radioactivity energy field to the content type.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param string $profile
   *   Profile type.
   * @param int|float $defaultEnergy
   *   Field energy when the entity is created.
   * @param int $granularity
   *   Energy decay granularity.
   * @param int $halfLifeTime
   *   Half life time.
   * @param int|float $cutoff
   *   Energy cut off value.
   * @param int $cardinality
   *   Field cardinality.
   */
  protected function createReferenceEnergyField(string $fieldName, string $profile, int|float $defaultEnergy = 0, int $granularity = 900, int $halfLifeTime = 43200, int|float $cutoff = 10, int $cardinality = 1): void {
    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'type' => 'radioactivity_reference',
      'field_name' => $fieldName,
      'cardinality' => $cardinality,
      'settings' => [
        'profile' => $profile,
        'granularity' => $granularity,
        'halflife' => $halfLifeTime,
        'cutoff' => $cutoff,
      ],
    ])->save();

    FieldConfig::create([
      'entity_type' => $this->entityType,
      'bundle' => $this->entityBundle,
      'field_name' => $fieldName,
      'required' => TRUE,
      'settings' => [
        'handler' => 'default:radioactivity',
        'handler_settings' => ['auto_create' => FALSE],
        'default_energy' => $defaultEnergy,
      ],
    ])->save();
  }

  /**
   * Creates an energy field formatter.
   *
   * @param string $fieldName
   *   Field machine name.
   */
  protected function createEnergyFormDisplay(string $fieldName): void {
    $entityFormDisplay = EntityFormDisplay::load('entity_test.entity_test.default');
    $entityFormDisplay->setComponent($fieldName, [
      'type' => 'radioactivity_energy',
    ]);
    $entityFormDisplay->save();
  }

  /**
   * Creates an emitter field formatter.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int|float $energy
   *   The energy to emit.
   * @param string $display
   *   The field display type.
   *
   * @return \Drupal\Core\Entity\Entity\EntityViewDisplay
   *   The entity view display object.
   */
  protected function createEmitterViewDisplay(string $fieldName, int|float $energy = 10, $display = TRUE): EntityViewDisplay {
    $entity_view_display = EntityViewDisplay::create([
      'targetEntityType' => $this->entityType,
      'bundle' => $this->entityBundle,
      'mode' => 'default',
      'status' => TRUE,
    ]);
    // @todo What type is $display? Docblock and method sig disagree.
    $entity_view_display->setComponent($fieldName, [
      'type' => 'radioactivity_emitter',
      'settings' => [
        'energy' => $energy,
        'display' => $display,
      ],
    ]);
    $entity_view_display->save();
    return $entity_view_display;
  }

  /**
   * Creates a Radioactivity field value formatter.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int $decimals
   *   Number of decimals to display.
   *
   * @return \Drupal\Core\Entity\Entity\EntityViewDisplay
   *   The entity view display object.
   */
  protected function createValueViewDisplay(string $fieldName, int $decimals = 0): EntityViewDisplay {
    $entity_view_display = EntityViewDisplay::create([
      'targetEntityType' => $this->entityType,
      'bundle' => $this->entityBundle,
      'mode' => 'default',
    ]);
    $entity_view_display->setComponent($fieldName, [
      'type' => 'radioactivity_value',
      'settings' => ['decimals' => $decimals],
    ]);
    $entity_view_display->save();
    return $entity_view_display;
  }

  /**
   * Creates an Radioactivity Reference field value formatter.
   *
   * @param string $fieldName
   *   Field machine name.
   * @param int $decimals
   *   Number of decimals to display.
   *
   * @return \Drupal\Core\Entity\Entity\EntityViewDisplay
   *   The entity view display object.
   */
  protected function createReferenceValueViewDisplay(string $fieldName, int $decimals = 0): EntityViewDisplay {
    $entity_view_display = EntityViewDisplay::create([
      'targetEntityType' => $this->entityType,
      'bundle' => $this->entityBundle,
      'mode' => 'default',
    ]);
    $entity_view_display->setComponent($fieldName, [
      'type' => 'radioactivity_value',
      'settings' => ['decimals' => $decimals],
    ]);
    $entity_view_display->save();
    return $entity_view_display;
  }

  /**
   * Set the entity type.
   *
   * @param string $type
   *   The entity type.
   */
  public function setEntityType(string $type): void {
    $this->entityType = $type;
  }

  /**
   * Set the entity bundle.
   *
   * @param string $bundle
   *   The entity bundle.
   */
  public function setEntityBundle(string $bundle): void {
    $this->entityBundle = $bundle;
  }

  /**
   * Sets the emitter energy of a field.
   *
   * @param string $fieldName
   *   The field name.
   * @param int $energy
   *   The energy value to set.
   */
  public function setFieldEmitterEnergy(string $fieldName, int $energy = 10): void {
    // @todo Shouldn't energy by int|float?
    $this->updateFieldEmitterSettings($fieldName, ['energy' => $energy]);
  }

  /**
   * Sets the emitter display mode of a field.
   *
   * @param string $fieldName
   *   The field name.
   * @param bool $displayEnergy
   *   Whether to display the energy level.
   */
  public function setFieldEmitterDisplay(string $fieldName, bool $displayEnergy = FALSE): void {
    $display = $displayEnergy;
    $this->updateFieldEmitterSettings($fieldName, ['display' => $display]);
  }

  /**
   * Updates the emitter field display settings.
   *
   * @param string $fieldName
   *   The field name.
   * @param array $settings
   *   Allowed keys:
   *   'energy': The energy value this field will emit when displayed.
   *   'display': True if the energy value is visible.
   */
  protected function updateFieldEmitterSettings(string $fieldName, array $settings): void {
    $display = EntityViewDisplay::load('entity_test.entity_test.default');
    $component = $display->getComponent($fieldName);

    foreach ($settings as $key => $value) {
      $component['settings'][$key] = $value;
    }
    $display->setComponent($fieldName, $component)
      ->save();
  }

  /**
   * Creates an entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The created entity.
   */
  public function createContent(): ContentEntityInterface {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = \Drupal::entityTypeManager()->getStorage($this->entityType)->create([
      'type' => $this->entityType,
      'title' => $this->randomString(),
    ]);
    $entity->save();

    return $entity;
  }

  /**
   * Asserts the actual incident count.
   *
   * @param int $expected
   *   The expected count.
   * @param string $message
   *   The assertion message.
   */
  public function assertIncidentCount(int $expected, string $message = ''): void {
    $actual = $this->getIncidentCount();
    $message = $message ?: $message = sprintf('The incident count is %s, but %s expected.', $actual, $expected);
    $this->assertTrue($actual == $expected, $message);
  }

  /**
   * Gets the number of incidents from the incident storage.
   *
   * @return int
   *   The incident count.
   */
  public function getIncidentCount(): int {
    $storage = \Drupal::service('radioactivity.default_incident_storage');
    return count($storage->getIncidents());
  }

}
