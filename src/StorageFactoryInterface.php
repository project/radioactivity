<?php

namespace Drupal\radioactivity;

/**
 * Storage factory service.
 */
interface StorageFactoryInterface {

  /**
   * Getter for classes which implement IncidentStorageInterface.
   *
   * @param string $type
   *   The type of storage to get.
   *
   * @return \Drupal\radioactivity\IncidentStorageInterface
   *   Instance of the requested storage.
   */
  public function get(string $type): IncidentStorageInterface;

  /**
   * Gets the configured incident storage.
   *
   * @return \Drupal\radioactivity\IncidentStorageInterface
   *   The configured storage instance.
   */
  public function getConfiguredStorage(): IncidentStorageInterface;

}
