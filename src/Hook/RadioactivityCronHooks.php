<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\radioactivity\RadioactivityProcessorInterface;

/**
 * Hook implementations used for scheduled execution.
 */
final class RadioactivityCronHooks {

  /**
   * Constructs a new RadioactivityCronHooks service.
   *
   * @param \Drupal\radioactivity\RadioactivityProcessorInterface $radioactivityProcessor
   *   The radioactivity.processor service.
   */
  public function __construct(
    protected RadioactivityProcessorInterface $radioactivityProcessor,
  ) {}

  /**
   * Implements hook_cron().
   */
  #[Hook('cron')]
  public function cron(): void {
    /** @var \Drupal\radioactivity\RadioactivityProcessorInterface $processor */
    $processor = $this->radioactivityProcessor;
    $processor->processDecay();
    $processor->processIncidents();
  }

  /**
   * Implements hook_queue_info_alter().
   */
  #[Hook('queue_info_alter')]
  public function queueInfoAlter(array &$queues): void {
    // Enforce the order of queue definitions to make sure that incidents are
    // processed after decays.
    $incidents = $queues['radioactivity_incidents'];
    unset($queues['radioactivity_incidents']);
    $queues['radioactivity_incidents'] = $incidents;
  }

}
