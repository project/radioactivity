<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Hook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;

/**
 * Hook implementations used to alter and enhance forms.
 */
final class RadioactivityFormHooks {
  use StringTranslationTrait;

  /**
   * Constructs a new RadioactivityFormHooks service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity_field.manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module_handler service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    TranslationInterface $string_translation,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_form_FORM_ID_alter() for 'field_config_edit_form'.
   */
  #[Hook('form_field_config_edit_form_alter')]
  public function fieldConfigEditFormAlter(array &$form, FormStateInterface $form_state): void {
    $fieldConfig = $form_state->getFormObject()->getEntity();
    if ($fieldConfig->getType() === 'radioactivity_reference') {
      // The radioactivity_reference field must always be "required".
      if (isset($form['required'])) {
        $form['required']['#disabled'] = TRUE;
        $form['required']['#default_value'] = TRUE;
      }
    }
  }

  /**
   * Implements hook_form_FORM_ID_alter() for 'field_ui_field_storage_add_form'.
   *
   * @todo Remove in 5.0.0 when the deprecated 'radioactivity' field gets removed.
   */
  #[Hook('form_field_ui_field_storage_add_form_alter')]
  public function fieldUiFieldStorageAddFormAlter(array &$form, FormStateInterface $form_state, string $form_id): void {
    $map = $this->entityFieldManager->getFieldMapByFieldType('radioactivity');
    if (empty($map)) {
      // Hide the deprecated radioactivity field type when no field of this
      // type is currently being used.
      unset($form['group_field_options_wrapper']['fields']['radioactivity']);
    }
    else {
      if ($form_state->getValue('new_storage_type') === 'trending') {
        // If there are existing uses of the deprecated radioactivity field,
        // provide guidance to help administrator choose which to use.
        $description_text = $this->t('Use <em>Radioactivity Reference</em> fields for all new sites. The <em>Radioactivity (deprecated)</em> field is available only on legacy sites that are already using this field.');
        if ($this->moduleHandler->moduleExists('help')) {
          $description_text .= ' ' . $this->t('For more information, see the <a href="@help_url">Radioactivity help page</a>.', [
            '@help_url' => Url::fromRoute('help.page', ['name' => 'radioactivity'])->toString(),
          ]);
        }
        $form['group_field_options_wrapper']['description_wrapper'] = [
          '#type' => 'item',
          '#markup' => $description_text,
        ];
      }
    }
  }

}
