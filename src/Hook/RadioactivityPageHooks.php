<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\radioactivity\StorageFactoryInterface;

/**
 * Hook implementations used for page rendering.
 */
final class RadioactivityPageHooks {

  /**
   * Constructs a new RadioactivityPageHooks service.
   *
   * @param \Drupal\radioactivity\StorageFactoryInterface $radioactivityStorage
   *   The radioactivity.storage service.
   */
  public function __construct(
    protected StorageFactoryInterface $radioactivityStorage,
  ) {}

  /**
   * Implements hook_page_attachments_alter().
   */
  #[Hook('page_attachments_alter')]
  public function pageAttachmentsAlter(array &$attachments): void {
    $this->radioactivityStorage
      ->getConfiguredStorage()
      ->injectSettings($attachments);
  }

}
