<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Hook implementations used to provide help.
 */
final class RadioactivityHelpHooks {
  use StringTranslationTrait;

  /**
   * Constructs a new RadioactivityHelpHooks service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    TranslationInterface $string_translation,
  ) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function help(string $route_name, RouteMatchInterface $route_match): ?string {
    switch ($route_name) {
      // Main module help for the radioactivity module.
      case 'help.page.radioactivity':
        $output = '';
        $output .= '<h3>' . $this->t('About') . '</h3>';
        $output .= '<p>' . $this->t('With the Radioactivity module you can measure popularity of your content. In combination with Views you can makes lists of popular content.') . '</p>';
        $output .= '<h3>' . $this->t('Configuration') . '</h3>';
        $output .= '<dl>';
        $output .= '<dt>' . $this->t('Add radioactivity field') . '</dt>';
        $output .= '<dd>' . $this->t('Two Radioactivity fields are available. When you start with Radioactivity module, you are advised to use the Radioactivity Reference field. The Radioactivity field (General section) is for existing sites and will de deprecated in the future.') . '</dd>';
        $output .= '<dd>' . $this->t('Attach the Radioactivity (Reference) field to your entity.') . '</dd>';
        $output .= '<dd>' . $this->t('Configure the field settings if required, default should be sufficient. A detailed explanation is available in README.txt.') . '</dd>';
        $output .= '<dt>' . $this->t('Manage form display') . '</dt>';
        $output .= '<dd>' . $this->t('Enable the widget to allow editors to change the energy value.') . '</dd>';
        $output .= '<dt>' . $this->t('Manage display') . '</dt>';
        $output .= '<dd>' . $this->t('Use the Emitter widget in the view modes that should be measured for popularity. Disable the widget in view modes that are not relevant for popularity.') . '</dd>';
        $output .= '<dd>' . $this->t('Configure the Emitter with the amount of energy that will be added per view.') . '</dd>';
        $output .= '<dt>' . $this->t('Use cron') . '</dt>';
        $output .= '<dd>' . $this->t('Cron must be enabled to update the energy levels.') . '</dd>';
        $output .= '</dl>';
        $output .= '<h3>' . $this->t('Using with views') . '</h3>';
        $output .= '<dl>';
        $output .= '<dt>' . $this->t('Configure display') . '</dt>';
        $output .= '<dd>' . $this->t('When using the Radioactivity Reference field, use the Relation ship "Radioactivity referenced from ..." to include the Radioactivity data into your Content view.') . '</dd>';
        $output .= '<dd>' . $this->t('To sorting the view on popularity sort the results on descending "energy".') . '</dd>';
        $output .= '</dl>';
        return $output;
    }
    return NULL;
  }

}
