<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Hook;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\radioactivity\EntityOperations;

/**
 * Hook implementations used to create and dispatch Entity Events.
 */
final class RadioactivityEntityHooks {

  /**
   * Constructs a new RadioactivityEntityHooks service.
   *
   * @param Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class_resolver service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   */
  public function __construct(
    protected ClassResolverInterface $classResolver,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * Implements hook_entity_load().
   */
  #[Hook('entity_load')]
  public function entityLoad(array $entities, string $type): void {
    // In order for the field formatters to be rendered we need to make sure
    // the field actually has something in it to trigger the formatters.
    $fields = $this->getFieldNames();

    /** @var \Drupal\Core\Entity\FieldableEntityInterface[] $entities */
    foreach ($entities as &$entity) {
      foreach ($fields as $field_name) {
        if (is_a($entity, FieldableEntityInterface::class)
            && $entity->hasField($field_name)) {
          if (!$entity->get($field_name)->energy) {
            $entity->get($field_name)->energy = 0;
            $entity->get($field_name)->timestamp = 0;
          }
        }
      }
    }
  }

  /**
   * Implements hook_entity_presave().
   */
  #[Hook('entity_presave')]
  public function entityPresave(EntityInterface $entity): void {
    $this->classResolver
      ->getInstanceFromDefinition(EntityOperations::class)
      ->entityPresave($entity);
  }

  /**
   * Get a list of Radioactivity field names.
   *
   * @return array
   *   An array of string field names.
   */
  protected function getFieldNames(): array {
    static $fields;

    // @todo Is static caching really necessary anymore?
    if (is_array($fields)) {
      return $fields;
    }

    $fields = [];

    /** @var \Drupal\field\Entity\FieldStorageConfig[] $field_storage_configs */
    if (!$field_storage_configs = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties(['type' => 'radioactivity'])) {
      return $fields;
    }

    // @todo Does this return duplicate entries if more than one bundle uses
    // the same field?
    foreach ($field_storage_configs as $field_storage) {
      $fields[] = $field_storage->get('field_name');
    }

    return $fields;
  }

}
