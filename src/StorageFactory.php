<?php

declare(strict_types=1);

namespace Drupal\radioactivity;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ClassResolverInterface;

/**
 * Storage factory service.
 */
class StorageFactory implements StorageFactoryInterface {

  /**
   * The radioactivity storage configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $storageConfig;

  /**
   * StorageFactory constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected ClassResolverInterface $classResolver,
  ) {
    $this->storageConfig = $configFactory->get('radioactivity.storage');
  }

  /**
   * {@inheritdoc}
   */
  public function get(string $type): IncidentStorageInterface {
    switch ($type) {
      case 'rest_local':
        $instance = $this->classResolver->getInstanceFromDefinition('radioactivity.rest_incident_storage');
        $instance->setEndpoint(NULL);
        break;

      case 'rest_remote':
        $instance = $this->classResolver->getInstanceFromDefinition('radioactivity.rest_incident_storage');
        $instance->setEndpoint($this->storageConfig->get('endpoint'));
        break;

      case 'default':
      default:
        $instance = $this->classResolver->getInstanceFromDefinition('radioactivity.default_incident_storage');
    }

    /** @var \Drupal\radioactivity\IncidentStorageInterface $instance */
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguredStorage(): IncidentStorageInterface {
    $type = $this->storageConfig->get('type') ?: 'default';
    return $this->get($type);
  }

}
