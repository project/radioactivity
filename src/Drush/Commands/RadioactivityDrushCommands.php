<?php

declare(strict_types=1);

namespace Drupal\radioactivity\Drush\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\radioactivity\RadioactivityReferenceUpdaterInterface;
use Drush\Attributes as CLI;
use Drush\Commands\AutowireTrait;
use Drush\Commands\DrushCommands;

/**
 * Drush 12+ commands for Radioactivity.
 */
final class RadioactivityDrushCommands extends DrushCommands {
  use AutowireTrait;

  /**
   * RadioactivityCommands constructor.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected RadioactivityReferenceUpdaterInterface $radioactivityReferenceUpdater,
  ) {
    parent::__construct();
  }

  /**
   * Update radioactivity references.
   *
   * @usage radioactivity:fix-references
   *   Fixes empty radioactivity reference fields by adding a reference.
   *
   * @command radioactivity:fix-references
   */
  #[CLI\Command(name: 'radioactivity:fix-references')]
  #[CLI\Help(description: 'Update radioactivity references.')]
  #[CLI\Usage(name: 'drush radioactivity:fix-references', description: 'Fixes empty radioactivity reference fields by adding a reference.')]
  public function radioactivityFixReferences(): void {

    $entitiesWithoutTarget = $this->radioactivityReferenceUpdater->getReferencesWithoutTarget();
    if (empty($entitiesWithoutTarget)) {
      $this->logger->warning(dt('No radioactivity reference fields found that need to be fixed.'));
      return;
    }

    // @todo Perform this in batches.
    foreach ($entitiesWithoutTarget as $item) {
      $entityStorage = $this->entityTypeManager->getStorage($item['entity_type']);
      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      $entity = $entityStorage->load($item['id']);
      $this->radioactivityReferenceUpdater->updateReferenceFields($entity);
    }

    $this->logger->success(dt('@count entities with radioactivity reference field fixed.', [
      '@count' => count($entitiesWithoutTarget),
    ]));
  }

}
