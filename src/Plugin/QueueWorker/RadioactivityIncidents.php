<?php

namespace Drupal\radioactivity\Plugin\QueueWorker;

use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Processes radioactivity emits.
 *
 * @QueueWorker(
 *   id = "radioactivity_incidents",
 *   title = @Translation("Process radioactivity incidents"),
 *   cron = {"time" = 10}
 * )
 */
#[QueueWorker(
  id: 'radioactivity_incidents',
  title: new TranslatableMarkup('Process radioactivity incidents'),
  cron: ['time' => 10]
)]
class RadioactivityIncidents extends RadioactivityQueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->radioactivityProcessor->queueProcessIncidents($data['entity_type'], $data['incidents']);
  }

}
