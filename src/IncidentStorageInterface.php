<?php

namespace Drupal\radioactivity;

/**
 * Defines the incident storage interface.
 */
interface IncidentStorageInterface {

  /**
   * Adds an incident to the storage.
   *
   * @param \Drupal\radioactivity\IncidentInterface $incident
   *   The incident object.
   */
  public function addIncident(IncidentInterface $incident): void;

  /**
   * Gets all incidents from the storage.
   *
   * @return \Drupal\radioactivity\IncidentInterface[]
   *   Array of incident objects.
   */
  public function getIncidents(): array;

  /**
   * Gets all incidents from the storage per entity type.
   *
   * @param string $entity_type
   *   Entity type for selection. Default to all entity types.
   *
   * @return \Drupal\radioactivity\IncidentInterface[][]
   *   Array of incident objects keyed by entity type (1st) and entity ID (2nd).
   */
  public function getIncidentsByType(string $entity_type = ''): array;

  /**
   * Clears the incident storage.
   */
  public function clearIncidents(): void;

  /**
   * Add endpoint settings to the page.
   *
   * @param array $page
   *   Page attachments as provided by hook_page_attachments_alter().
   */
  public function injectSettings(array &$page): void;

}
