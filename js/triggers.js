/**
 * @file
 * Adds radioactivity triggers.
 */

(function ($, Drupal, drupalSettings) {
  /**
   * Add Radioactivity triggers.
   */
  Drupal.behaviors.radioactivityTriggers = {
    attach(context) {
      // Only run on page load, not for subsequent ajax loads.
      if (
        typeof context === 'object' &&
        context.toString().indexOf('HTMLDocument') !== -1
      ) {
        const emits = [];
        let emit;
        let i = 0;

        emit = drupalSettings[`ra_emit_${i}`];
        while (emit) {
          emits.push(JSON.parse(emit));
          i++;
          emit = drupalSettings[`ra_emit_${i}`];
        }

        $.ajax({
          type: 'POST',
          url: drupalSettings.radioactivity.endpoint,
          data: JSON.stringify(emits),
          success(data) {
            if (data.status !== 'ok') {
              console.error(`Radioactivity: ${data.message}`);
            }
          },
          dataType: 'json',
        });
      }
    },
  };
})(jQuery, Drupal, drupalSettings);
